all:
	cmake -B build -S .
	cmake --build build --parallel

run: all
	cmake --build build --parallel --target run

entr:
	ls *.cxx CMakeLists.txt | entr -cr make

clean:
	cmake --build build --target clean
