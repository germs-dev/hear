#include <cassert>
#include <cmath>
#include <fstream>
#include <numbers>
#include <ranges>
#include <string_view>



void generate_tones(std::string_view file) {

  // Each test tone shall run for a fixed number of seconds
  constexpr auto tone_duration{1uz};

  // namespace {
  struct  {
    unsigned int riff_id;
    unsigned int riff_size;
    unsigned int wave_tag;
    unsigned int format_id;
    unsigned int format_size;
    unsigned int format_tag : 16;
    unsigned int channels : 16;
    unsigned int sample_rate;
    unsigned int bytes_per_second;
    unsigned int block_align : 16;
    unsigned int bit_depth : 16;
    unsigned int data_id;
    unsigned int data_size;
  } wav{
      .riff_id = 0x4646'4952,
      .riff_size = 0x8000'0024,
      .wave_tag = 0x4556'4157,
      .format_id = 0x2074'6D66,
      .format_size = 16,
      .format_tag = 1,
      .channels = 2,
      .sample_rate = 48000,
      .bytes_per_second = wav.sample_rate * wav.bit_depth / 8,
      .block_align = 1,
      .bit_depth = 16,
      .data_id = 0x6174'6164,
      .data_size = 0x8000'0000,
  };
 
  assert(sizeof wav == 44);

  // Create a file to write to
  auto out = std::ofstream{file.data()};

  // Declare channels and frequencies
  constexpr std::array channels{"left", "right"};
  constexpr std::array frequencies{60.0,   100.0,  150.0, 400.0,
                                   1000.0, 2400.0, 5000.0};
  constexpr std::array volumes{0.001, 0.005, 0.0075, 0.01, 0.02, 0.1, 0.2};

  // Write the WAV header
  out.write(reinterpret_cast<char *>(&wav), sizeof wav);

  // Generate audio for all tests
  for (const auto [x, channel] : std::views::enumerate(channels))
    for (const auto [y, freq] : std::views::enumerate(frequencies))
      for (const auto [z, volume] :
           std::views::enumerate(volumes) | std::views::reverse) {

        // Play a second of silence into each channel
        for (unsigned long i = 0; i < wav.sample_rate * 2; ++i) {
          unsigned short quiet = 0;
          out.write(reinterpret_cast<char *>(&quiet), sizeof quiet);
        }

        assert(freq > 0.0);

        // Create a second's worth of samples
        const ssize_t count = wav.sample_rate * tone_duration;

        for (const auto i : std::views::iota(0, count)) {

          const double phase = 2.0 * std::numbers::pi_v<double> * i;
          constexpr double max_volume = double{0xffff} / 256.0;
          const double current_volume = max_volume * volume;

          // Samples per complete cycle
          const double interval = 1.0 * wav.sample_rate / freq;

          if (x % 2) {
            unsigned short quiet = 0;
            out.write(reinterpret_cast<char *>(&quiet), sizeof quiet);
          }

          unsigned short sample = static_cast<unsigned short>(
              std::llround(sin(phase / interval) * current_volume));

          // 2's comp
          sample = ~sample + 1;

          out.write(reinterpret_cast<char *>(&sample), sizeof sample);

          if (not(x % 2)) {
            unsigned short quiet = 0;
            out.write(reinterpret_cast<char *>(&quiet), sizeof quiet);
          }
        }
      }
}