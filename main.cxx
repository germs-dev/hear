#include <barrier>
#include <chrono>
#include <fmt/core.h>
#include <fstream>
#include <iostream>
#include <ranges>
#include <string_view>
#include <thread>

/// hearmenow is a hearing test application that asks the user for input when
/// audio is heard, produces stats and a profile which can bel plugged into an
/// exqualiser application. Audio is sent to stdout as a binary stream -- this
/// needs to be piped to aplay or similar or the binary will come out on the
/// terminal :( -- and helpful debug is sent to stderr so you can see what's
/// going on.

// Forward declarations
void generate_tones(std::string_view);

/// Create barrier for threads to synchronise
std::barrier b(2, [] { fmt::print(stderr, "All threads synchronised\n"); });

int main() {

  // Generate test tones
  constexpr auto tone_file{+"tones.wav"};
  fmt::print(stderr, "Generating test tones, writing to '{}'\n", tone_file);
  generate_tones(tone_file);

  // Start key logging thread
  std::jthread key_thread([&](std::stop_token stop_token) {
    fmt::print(stderr, "Starting key logger\n");
    b.arrive_and_wait();

    fmt::print(
        stderr,
        "Hit 'return' when you first hear a tone for each frequency...\n");

    // Do some work until told otherwise
    while (not stop_token.stop_requested()) {

      // Process a character from stdin
      char c = std::cin.get();

      // Get time before generating the tone
      using namespace std::chrono;
      const auto before = steady_clock::now();

      fmt::print(
          stderr, "\tMarker @ {}\n",
          before.time_since_epoch().count());
    }

    fmt::print("Quitting key logger\n");
  });

  // Set up test tones
  auto in = std::ifstream{tone_file, std::ios::binary};

  // Synchronise with key logger
  b.arrive_and_wait();

  // Stream audio
  std::cout << in.rdbuf();

  // Catch up with the key logger
  key_thread.request_stop();
  fmt::print(stderr, "cya\n");
}
